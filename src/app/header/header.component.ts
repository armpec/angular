import { Component, OnInit } from '@angular/core';
import {UserService} from "../core/services/user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  fullName: string = '';

  isLoggedIn: boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.isLoggedIn = this.userService.isLoggedIn();
    this.fullName = this.userService.getLoggedInUser().firstName + ' ' + this.userService.getLoggedInUser().lastName;
  }
}
