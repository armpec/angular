import {Component, OnInit} from '@angular/core';
import {CarService} from "../core/services/car.service";
import {Car} from "../core/models/car.model";
import {Company} from "../core/models/company.model";
import {CompanyService} from "../core/services/company.service";

@Component({
  selector: 'app-car-index',
  templateUrl: './car-index.component.html',
  styleUrls: ['./car-index.component.css']
})
export class CarIndexComponent implements OnInit {

  cars: Car[] = [];

  companies: Company[] = [];

  constructor(private carService: CarService, private companyService: CompanyService) {
  }

  ngOnInit(): void {
    this.carService.getCars().subscribe(data => {
      this.cars = data;
    });

    this.companyService.getCompanies().subscribe(data => {
      this.companies = data;
    });
  }

  filterByCompany(event: any): void {
    this.carService.getCarsByCompany(event.target.value).subscribe(data => {
      this.cars = data;
    });
  }

}
