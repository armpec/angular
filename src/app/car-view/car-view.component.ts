import { Component, OnInit } from '@angular/core';
import {CarService} from "../core/services/car.service";
import {ActivatedRoute} from "@angular/router";
import {Car} from "../core/models/car.model";
import {User} from "../core/models/user.model";
import {UserService} from "../core/services/user.service";

@Component({
  selector: 'app-car-view',
  templateUrl: './car-view.component.html',
  styleUrls: ['./car-view.component.css']
})
export class CarViewComponent implements OnInit {

  car: Car;

  owner: User;

  constructor(private carService: CarService,
              private userService: UserService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.carService.getCar(this.route.snapshot.paramMap.get('id')).subscribe(data => {
      this.car = data;

      if (this.car.ownerId) {
        this.userService.getUser(this.car.ownerId).subscribe(data => {
          this.owner = data;
        });
      }
    });
  }

}
