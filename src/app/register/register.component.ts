import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../core/services/user.service";
import {Router} from "@angular/router";
import {CompanyService} from "../core/services/company.service";
import {Company} from "../core/models/company.model";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  companies: Company[];

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private companyService: CompanyService,
              private router: Router) {
    this.registerForm = this.fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required],
      'confirmPassword': ['', Validators.required],
      'companyId': ['', Validators.required],
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'age': ['', Validators.required],
      'isAdmin': [true, Validators.required]
    });
  }

  ngOnInit(): void {
    this.companyService.getCompanies().subscribe(data => {
      this.companies = data;
    })
  }

  onSubmit() {
    this.userService.register(this.registerForm.value).subscribe(data => {
      this.router.navigate(['/login']);
    });
  }
}
