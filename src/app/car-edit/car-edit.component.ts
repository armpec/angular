import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CarService} from "../core/services/car.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Car} from "../core/models/car.model";

@Component({
  selector: 'app-car-edit',
  templateUrl: './car-edit.component.html',
  styleUrls: ['./car-edit.component.css']
})
export class CarEditComponent implements OnInit {

  car: Car;

  carForm: FormGroup;

  constructor(private fb: FormBuilder,
              private carService: CarService,
              private route: ActivatedRoute,
              private router: Router) {
    this.carForm = this.fb.group({
      'manufacturer': ['', Validators.required],
      'model': ['', Validators.required],
      'year': ['', Validators.required],
      'color': ['', Validators.required],
      'description': ['', Validators.required],
      'value': ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.carService.getCar(this.route.snapshot.paramMap.get('id')).subscribe(data => {
      this.car = data;

      this.carForm.setValue({
        'manufacturer': data.manufacturer,
        'model': data.model,
        'year': data.year,
        'color': data.color,
        'description': data.description,
        'value': data.value
      });
    });
  }

  onSubmit(): void {
    this.carService.updateCar(this.car.id, this.carForm.value).subscribe(data => {
      this.router.navigate(['/cars/' + this.car.id + '/view'])
    });
  }

  onDelete(): void {
    this.carService.deleteCar(this.car.id).subscribe(data => {
      this.router.navigate(['/cars/'])
    });
  }

}
