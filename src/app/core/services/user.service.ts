import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {Observable} from "rxjs";
import {Car} from "../models/car.model";
import {User} from "../models/user.model";
import {JwtService} from "./jwt.service";
import {Registration} from "../models/registration.model";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private router: Router,
              private apiService: ApiService,
              private jwtService: JwtService) { }

  getUser(id: any): Observable<User> {
    return this.apiService.get('/users/' + id);
  }

  getUserCars(id: any): Observable<Car[]> {
    return this.apiService.get('/users/' + id + '/cars');
  }

  login(username: string, password: string): void {
    this.apiService.post('/login', {username: username, password: password}).subscribe(data => {
      localStorage.setItem('user', JSON.stringify(data.user));
      this.jwtService.saveToken(data.accessToken);
      this.router.navigate(['/']);
    });
  }

  register(registration: Registration): Observable<any> {
    return this.apiService.post('/register', registration);
  }

  getLoggedInUser(): User {
    return JSON.parse(localStorage.getItem('user') || '');
  }

  isLoggedIn(): boolean {
    return localStorage.getItem('user') !== null;
  }
}
