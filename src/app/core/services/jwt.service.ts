import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor() { }

  getToken(): String {
    return localStorage['accessToken'];
  }

  saveToken(token: String) {
    localStorage['accessToken'] = token;
  }

  destroyToken() {
    localStorage.removeItem('accessToken');
  }

}
