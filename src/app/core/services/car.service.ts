import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Car} from "../models/car.model";
import {ApiService} from "./api.service";
import {HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private apiService: ApiService) { }

  getCars(): Observable<Car[]> {
    return this.apiService.get('/cars');
  }

  getCarsByCompany(companyId: number): Observable<Car[]> {
    return this.apiService.get('/cars', new HttpParams().set('companyId', companyId));
  }

  getCar(id: any): Observable<Car> {
    return this.apiService.get('/cars/' + id);
  }

  updateCar(id: any, car: Car): Observable<Car> {
    return this.apiService.put('/cars/' + id, car);
  }

  deleteCar(id: any): Observable<any> {
    return this.apiService.delete('/cars/' + id);
  }
}
