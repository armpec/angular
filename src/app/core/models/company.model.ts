export interface Company {
  id: number;
  name: string;
  created: string;
  updated: string;
}
