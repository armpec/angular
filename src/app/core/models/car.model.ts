import {Company} from "./company.model";

export interface Car {
  id: number;
  ownerId: number;
  company: Company;
  manufacturer: string;
  model: string;
  year: number;
  color: string;
  description: string;
  value: number;
  created: string;
  updated: string;
}
