export interface Registration {
  companyId: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  age: number;
  isAdmin: boolean;
}
