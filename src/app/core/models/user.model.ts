import {Role} from "./role.model";

export interface User {
  id: number;
  companyId: number;
  username: string;
  firstName: string;
  lastName: string;
  age: number;
  roles: Role[];
  isAdmin: boolean;
  created: string;
  updated: string;
}
