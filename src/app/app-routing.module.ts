import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {CarIndexComponent} from "./car-index/car-index.component";
import {CarViewComponent} from "./car-view/car-view.component";
import {CarEditComponent} from "./car-edit/car-edit.component";
import {CarCreateComponent} from "./car-create/car-create.component";

const routes: Routes = [
  {path: '', redirectTo: 'cars', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'cars', component: CarIndexComponent},
  {path: 'cars/:id/view', component: CarViewComponent},
  {path: 'cars/:id/edit', component: CarEditComponent},
  {path: 'cars/create', component: CarCreateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
